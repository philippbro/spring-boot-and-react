const formattedDate = (timestamp) => {
    const date = new Date(timestamp);
    return date.getDate() + "." + date.getMonth() + "." + date.getFullYear()
};

export const formattedPrice = (priceInCent, currencySymbol) => {
    const formattedPrice = priceInCent / 100;
    return formattedPrice.toFixed(2) + " " + currencySymbol;
};

export const getPriceDropText = (priceInCent, originalPrice, currencySymbol, timestamp) => {
    const priceDrop = (priceInCent - originalPrice);
    if (priceDrop === 0) {
        return "price not changed"
    }
    const priceDropText = formattedPrice(priceDrop, currencySymbol);
    const formattedPriceDropText = priceDropText + " since " + formattedDate(timestamp);
    if (priceDrop > 0) {
        return "+" + formattedPriceDropText;
    }
    return formattedPriceDropText;
};
