import React from "react";
import ReactDOM from "react-dom";

import "../scss/index.scss";
import App from "./App.js";
import {library} from "@fortawesome/fontawesome-svg-core";
import {fas as freeIcons} from "@fortawesome/free-solid-svg-icons";

library.add(freeIcons);

ReactDOM.render(
    <App/>,
    document.getElementById("root")
);
