import React, {Component} from "react";
import axios from "axios/index";
import "../scss/components/App.scss";
import Welcome from "./components/headline/Welcome";
import SettingsSection from "./components/settings-section/SettingsSection";
import WishListContainer from "./components/wishlist-container/WishListContainer";
import {filteredByTextSearch} from "./components/settings-section/text-search/SearchItems";
import {sorted} from "./components/settings-section/sort/SortItems";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            wishlist: [],
            selectedLayout: "",
            search: "",
            sort: "",
        }
    }

    componentWillMount() {
        axios.get('http://localhost:8089/data')
            .then(response => {
                this.setState({
                    wishlist: response.data.wishlist,
                    selectedLayout: response.data.userConfig.layout
                });
            });
    }

    handleLayout(selectedLayout) {
        this.setState({
            selectedLayout: selectedLayout
        });
    };

    updateSearch(event) {
        // limited to 20 characters
        this.setState({
            search: event.target.value.substr(0,20)
        });
    }

    updateSort(sort) {
        this.setState({
            sort: sort
        })
    }

    render() {
        const layout = this.state.selectedLayout;
        const wishlist = this.state.wishlist;
        const wishlistSize = wishlist.length;
        const searchValue = this.state.search;
        const sort = this.state.sort;
        const sortedWishlist = sorted(sort, wishlist);
        const filteredWishlist = filteredByTextSearch(sortedWishlist, searchValue);

        return (
            <div className="App">
                <Welcome itemCount={wishlistSize}/>
                <SettingsSection
                    layout={layout}
                    onLayoutChange={(layout) => this.handleLayout(layout)}

                    searchValue={searchValue}
                    updateSearch={this.updateSearch.bind(this)}

                    sort={sort}
                    onSortChange={(sort) => this.updateSort(sort)}
                />
                <WishListContainer
                    wishlist={filteredWishlist}
                    layout={layout}
                    searchValue={searchValue}
                />
            </div>
        );
    }
}

export default App;
