import React from "react";
import { StyleSheet, css } from 'aphrodite';
import ListFront from "./ListFront";

const ListItem = ({wishlist}) => {

    return (
        wishlist.map(item =>
            <div className={css(styles.listItemWrapper, styles.animation)} key={item.id}>
                <ListFront item={item}/>
            </div>
        )
    )
};

export default ListItem;

const slideInKeyframes = {
    'from': {
        opacity: 0,
        transform: 'translate3d(0, -10%, 0)',
        visibility: 'visible',
    },

    'to': {
        opacity: 1,
        transform: 'translate3d(0, 0, 0)',
    }
};

const styles = StyleSheet.create({
    listItemWrapper: {
        padding: '10px',
        marginBottom: '10px',
        backgroundColor: 'white',
        boxShadow: '0 0 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        transition: 'all .5s cubic-bezier(.25,.8,.25,1)',
        ':hover': {
            boxShadow: '0 3px 7px rgba(0,0,0,0.25), 0 3px 8px rgba(0,0,0,0.22)'
        },
    },
    animation: {
        animationDuration: '.5s',
        animationFillMode: 'both',
        animationName: slideInKeyframes,
    }
});