import React from "react";
import { StyleSheet, css } from 'aphrodite';
import {FontAwesomeIcon as IconLib} from "@fortawesome/react-fontawesome";
import "../../../../scss/components/settingssection/layoutswitch/LayoutSwitch.scss";

export const LAYOUT = {
    LIST : "list",
    TILES: "tiles",
};

const LayoutSwitch = ({layout, onLayoutChange}) => {

    return(
        <div className="LayoutSwitch">
            <span className={layout}>
                <IconLib icon="bars" onClick={() => onLayoutChange(LAYOUT.LIST)}/>
                <IconLib icon="th-large" onClick={() => onLayoutChange(LAYOUT.TILES)}/>
            </span>
        </div>
    )
};

export default LayoutSwitch;

// TODO: how to style cascading with aphrodite?
const styles = StyleSheet.create({
    layoutSwitch: {
        fontSize: '26px',
        color: 'darkgrey',
        'svg': {
            margin: '5px',
            cursor: 'pointer',
            ':hover': {
                color: 'cornflowerblue',
                transform: 'scale(1.1)',
            }
        },
        '.list svg.fa-bars,.tiles svg.fa-th-large,.shuffle svg.fa-th-list': {
            color: 'cornflowerblue',
        },
    },
});
